package omsu.javaprojects.functional;

import omsu.javaprojects.arity.Div1Arity;
import omsu.javaprojects.arity.Function1Arity;
import omsu.javaprojects.arity.Linear1Arity;
import omsu.javaprojects.arity.Sin1Arity;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BoundSumFuncTest {
    @Test
    public void testBoundsLin() throws FunctionalExeption {
        BoundSumFunc func = new BoundSumFunc(0,1);
        Function1Arity lin = new Linear1Arity(1,1,-1,2);
        assertEquals(4.5,func.functional(lin),0.1);
    }
    @Test
    public void testBoundsDiv() throws FunctionalExeption {
        BoundSumFunc func = new BoundSumFunc(0,1);
        Function1Arity div = new Div1Arity(1,1,1,1,-1,2);
        assertEquals(3.0,func.functional(div),0.1);
    }
    @Test
    public void testBoundsSin() throws FunctionalExeption {
        BoundSumFunc func = new BoundSumFunc(0,1);
        Function1Arity sin = new Sin1Arity(1,1,-1,2);
        assertEquals(1.3,func.functional(sin),0.1);
    }

}
