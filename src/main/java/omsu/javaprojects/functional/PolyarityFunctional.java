package omsu.javaprojects.functional;

import omsu.javaprojects.arity.Function1Arity;

public interface PolyarityFunctional<T extends Function1Arity>{
    double functional(T func) throws FunctionalExeption;
}
